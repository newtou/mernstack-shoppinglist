import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import AppNavBar from "./components/AppNavBar";
import ShoppingList from "./components/shoppinglist/ShoppingList";
import { Provider } from "react-redux";
import { Store } from "./store/Store";
import ItemsModal from "./components/modal/ItemsModal";
import { Container } from "reactstrap";
import { loadUser } from "./store/actions/authActions";

function App() {
  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      Store.dispatch(loadUser());
    }

    return () => (isMounted = false);
  }, []);
  return (
    <Provider store={Store}>
      <div className="App">
        <AppNavBar></AppNavBar>
        <Container>
          <ItemsModal></ItemsModal>
          <ShoppingList></ShoppingList>
        </Container>
      </div>
    </Provider>
  );
}

export default App;
