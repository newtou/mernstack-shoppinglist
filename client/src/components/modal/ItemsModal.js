import React, { useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import { connect } from "react-redux";
import { addItem } from "../../store/actions/itemActions";
import PropTypes from "prop-types";

function ItemsModal({ addItem, isAuthenticated }) {
  const [isOpen, setIsOpen] = useState(false);
  const [value, setValue] = useState({
    name: ""
  });

  const toggle = () => {
    setIsOpen(!isOpen);
  };
  const handleChange = e => {
    setValue({ [e.target.name]: e.target.value });
  };

  const handleSubmit = async e => {
    e.preventDefault();

    const newItem = {
      name: value.name
    };

    // add item via addItem action
    await addItem(newItem);

    setValue({ name: "" });

    // close modal
    toggle();
  };
  return (
    <div>
      {isAuthenticated ? (
        <Button color="dark" style={{ marginBottom: "2rem" }} onClick={toggle}>
          Add Item
        </Button>
      ) : (
        <h4 className="mb-3 ml-4">Please login to manage items</h4>
      )}

      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Add to shopping list</ModalHeader>
        <ModalBody>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="item">Item</Label>
              <Input
                type="text"
                name="name"
                value={value.name}
                placeholder="Add Item"
                onChange={handleChange}
              ></Input>
              <Button color="dark" style={{ marginTop: "2rem" }} block>
                Add Item
              </Button>
            </FormGroup>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}

ItemsModal.propTypes = {
  isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
  items: state.items,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { addItem })(ItemsModal);
