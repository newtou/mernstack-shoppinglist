import React, { useEffect } from "react";
import "./shopping.css";
import { Container, ListGroup, ListGroupItem, Button } from "reactstrap";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { connect } from "react-redux";
import { getItems, deleteItem } from "../../store/actions/itemActions";
import PropTypes from "prop-types";

function ShoppingList({ items, getItems, deleteItem, isAuthenticated }) {
  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      getItems();
    }
  }, [getItems]);

  const handleDelete = id => {
    deleteItem(id);
  };
  return (
    <Container>
      <ListGroup>
        <TransitionGroup className="shopping-list">
          {items.items.map(({ _id, name }) => (
            <CSSTransition key={_id} timeout={500} classNames="fade">
              <ListGroupItem>
                {isAuthenticated ? (
                  <Button
                    className="remove-btn"
                    color="danger"
                    size="sm"
                    onClick={() => handleDelete(_id)}
                  >
                    &times;
                  </Button>
                ) : null}

                {name}
              </ListGroupItem>
            </CSSTransition>
          ))}
        </TransitionGroup>
      </ListGroup>
    </Container>
  );
}

ShoppingList.propTypes = {
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
  items: state.items,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { getItems, deleteItem })(ShoppingList);
