import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
  NavLink
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { register } from "../../store/actions/authActions";
import { clearErrors } from "../../store/actions/errorActions";

function RegisterModal({ register, error, clearErrors, isAuthenticated }) {
  const [isOpen, setIsOpen] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [msg, setMsg] = useState(null);

  const toggle = () => {
    // clear errors
    clearErrors();
    setMsg(null);
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    let isMounted = true;

    if (isMounted) {
      if (error.id === "REGISTER_FAIL") {
        setMsg(error.msg.msg);
      }
      if (isOpen) {
        if (isAuthenticated) {
          setIsOpen(!isOpen);
        }
      }
    }
    return () => (isMounted = false);
  }, [error.msg, error.id, isAuthenticated, isOpen]);

  const handleSubmit = async e => {
    e.preventDefault();

    const newUser = {
      name,
      email,
      password
    };

    register(newUser);

    setName("");
    setEmail("");
    setPassword("");
  };
  return (
    <div>
      <NavLink onClick={toggle} href="#">
        Register
      </NavLink>
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Register</ModalHeader>
        <ModalBody>
          {msg ? <Alert color="danger">{msg}</Alert> : null}
          <Form onSubmit={handleSubmit}>
            <FormGroup className="mb-2">
              <Label for="name">Name</Label>
              <Input
                type="text"
                name="name"
                id="name"
                defaultValue={name}
                placeholder="name"
                onChange={e => setName(e.target.value)}
              ></Input>
            </FormGroup>
            <FormGroup className="mb-2">
              <Label for="email">Email</Label>
              <Input
                type="email"
                name="email"
                id="email"
                defaultValue={email}
                placeholder="email"
                onChange={e => setEmail(e.target.value)}
              ></Input>
            </FormGroup>
            <FormGroup className="mb-2">
              <Label for="password">Password</Label>
              <Input
                type="password"
                name="password"
                id="password"
                defaultValue={password}
                placeholder="password"
                onChange={e => setPassword(e.target.value)}
              ></Input>
            </FormGroup>
            <Button
              color="dark"
              style={{ marginTop: "2rem" }}
              onClick={handleSubmit}
              block
            >
              Register
            </Button>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}

RegisterModal.propTypes = {
  isAuthenticated: PropTypes.bool,
  error: PropTypes.object.isRequired,
  register: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(mapStateToProps, { register, clearErrors })(
  RegisterModal
);
