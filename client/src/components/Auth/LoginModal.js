import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Alert,
  NavLink
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { login } from "../../store/actions/authActions";
import { clearErrors } from "../../store/actions/errorActions";

function LoginModal({ login, error, clearErrors, isAuthenticated }) {
  const [isOpen, setIsOpen] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [msg, setMsg] = useState(null);

  const toggle = () => {
    // clear errors
    setMsg(null);
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    let isMounted = true;

    if (isMounted) {
      if (error.id === "LOGIN_FAIL") {
        setMsg(error.msg.msg);
      }
      if (isOpen) {
        if (isAuthenticated) {
          setIsOpen(!isOpen);
        }
      }
    }
    return () => (isMounted = false);
  }, [error.msg, error.id, isAuthenticated, isOpen]);

  const handleSubmit = async e => {
    e.preventDefault();

    const user = {
      email,
      password
    };

    await login(user);

    setEmail("");
    setPassword("");
  };
  return (
    <div>
      <NavLink onClick={toggle} href="#">
        Login
      </NavLink>
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Login</ModalHeader>
        <ModalBody>
          {msg ? <Alert color="danger">{msg}</Alert> : null}
          <Form onSubmit={handleSubmit}>
            <FormGroup className="mb-2">
              <Label for="email">Email</Label>
              <Input
                type="email"
                name="email"
                id="email"
                defaultValue={email}
                placeholder="email"
                onChange={e => setEmail(e.target.value)}
              ></Input>
            </FormGroup>
            <FormGroup className="mb-2">
              <Label for="password">Password</Label>
              <Input
                type="password"
                name="password"
                id="password"
                defaultValue={password}
                placeholder="password"
                onChange={e => setPassword(e.target.value)}
              ></Input>
            </FormGroup>
            <Button
              color="dark"
              style={{ marginTop: "2rem" }}
              onClick={handleSubmit}
              block
            >
              Login
            </Button>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}

LoginModal.propTypes = {
  isAuthenticated: PropTypes.bool,
  error: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(mapStateToProps, { login, clearErrors })(LoginModal);
