import React from "react";
import { logout } from "../../store/actions/authActions";
import { connect } from "react-redux";
import { NavLink } from "reactstrap";
import PropTypes from "prop-types";

function Logout({ logout }) {
  return (
    <>
      <NavLink onClick={logout} href="#">
        Logout
      </NavLink>
    </>
  );
}

Logout.propTypes = {
  logout: PropTypes.func.isRequired
};

export default connect(null, { logout })(Logout);
