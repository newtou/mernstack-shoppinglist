import { combineReducers } from "redux";
import { itemReducer } from "./itemReducer";
import { errorReducer } from "./ErrorReducer";
import { authReducer } from "./AuthReducer";

export const rootReducer = combineReducers({
  items: itemReducer,
  auth: authReducer,
  error: errorReducer
});
