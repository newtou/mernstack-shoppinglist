const router = require("express").Router();
const Item = require("../../models/Item");
const { authVerify } = require("../../middleware/verifyToken");

// @route GET api/items
// @desc Get All Items
// @access Public

router.get("/", async (req, res) => {
  try {
    const items = await Item.find().sort({ date: -1 });
    res.json(items);
  } catch (error) {
    console.log(error);
    res.status(400).send("Houston we have a problem");
  }
});

// @route POST api/items
// @desc Create an item
// @access Public

router.post("/", authVerify, async (req, res) => {
  const newItem = new Item({
    name: req.body.name
  });
  try {
    const item = await newItem.save();
    res.json(item);
  } catch (error) {
    console.log(error);
    res.status(400).send("Houston we have problem");
  }
});

// @route DELETE api/items/:id
// @desc DELETE an item
// @access Public

router.delete("/:id", authVerify, async (req, res) => {
  try {
    const item = await Item.findById(req.params.id);
    const deletedItem = await item.remove();
    res.json({ success: true });
  } catch (error) {
    console.log(error);
    res.status(404).send({ success: false });
  }
});

module.exports = router;
