const router = require("express").Router();
const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {
  registerValidation,
  loginValidation
} = require("../../validation/UserValidation");
const { authVerify } = require("../../middleware/verifyToken");

// @route POST api/user
// @desc Register new user
// @access Public

router.post("/register", async (req, res) => {
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).json(error.details[0].message);

  // check if user exists
  const emailExists = await User.findOne({ email: req.body.email });

  if (emailExists) return res.status(400).json("Email already exists");

  //create hashed password
  const salt = await bcrypt.genSalt(12);
  const hashPass = await bcrypt.hash(req.body.password, salt);

  // create new user
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashPass
  });

  try {
    const savedUser = await user.save();
    // create and assign token
    const token = jwt.sign(
      { _id: savedUser._id, email: savedUser.email, name: savedUser.name },
      process.env.JWT_KEY,
      { expiresIn: 3600 }
    );
    res.json({
      token,
      user: {
        id: savedUser.id,
        name: savedUser.name,
        email: savedUser.email
      }
    });
  } catch (error) {
    res.status(400).json({ error });
  }
});

// @route POST api/user
// @desc LOGIN new user
// @access Public

router.post("/login", async (req, res) => {
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // check if email exists
  const existingUser = await User.findOne({ email: req.body.email });

  if (!existingUser) return res.status(400).send("Email or password is wrong");

  const validPass = await bcrypt.compare(
    req.body.password,
    existingUser.password
  );

  if (!validPass) return res.status(400).send("Invalid password");

  // create and assign token
  const token = jwt.sign(
    {
      _id: existingUser._id,
      email: existingUser.email,
      name: existingUser.name
    },
    process.env.JWT_KEY
  );
  res.header("auth-token", token).send({
    token,
    user: {
      id: existingUser.id,
      name: existingUser.name,
      email: existingUser.email
    }
  });
});

// @route GET api/user/getuser
// @desc GET user
// @access Private

router.get("/", authVerify, async (req, res) => {
  const currentUser = await User.findById(req.user._id).select("-password");
  res.json(currentUser);
});

module.exports = router;
