const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
require("dotenv").config();

const AuthRoute = require("./server/routes/api/User");
const ItemsRoute = require("./server/routes/api/Items");

const app = express();

//express middleware
app.use(express.json());

// mongodb connection
const dbURL = require("./server/config/Keys").mongoURI;

mongoose.connect(
  dbURL,
  {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  error => {
    if (error) {
      console.log(error);
      return;
    }
    console.log("Mongo db is running...");
  }
);

app.use("/api/items", ItemsRoute);
app.use("/api/user", AuthRoute);

// server static assets if in production
if (process.env.NODE_ENV === "production") {
  // set static folder
  app.use(express.static("client/build"));
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server is running on port ${port}`));
